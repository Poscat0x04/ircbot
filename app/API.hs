{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE RecordWildCards #-}

module API where

import Config
import Control.Monad.IO.Class
import Data.ByteString (ByteString)
import qualified Data.ByteString.Lazy as LBS
import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Encoding (decodeUtf8, encodeUtf8)
import Lens.Micro
import Network.HTTP.Client (RequestBody (RequestBodyBS))
import Network.HTTP.Client.MultipartFormData
import Network.HTTP.Req
import Servant.Client (ClientError)
import qualified Text.HTML.TagSoup as S
import qualified Text.URI as URI
import Types
import Web.Pixiv.API
import Web.Pixiv.Download
import Web.Pixiv.Types
import Web.Pixiv.Types.Lens

fetchInfo :: Int -> IRCBot (Either ClientError Illust)
fetchInfo illustId = runPixivInIRC "fetchInfo" $ getIllustDetail illustId

fetchRandomRelated :: Int -> IRCBot (Either ClientError Illust)
fetchRandomRelated illustId = runPixivInIRC "fetchRandomRelated" $ randomP (getIllustRelated illustId 1)

fetchSearch :: Text -> IRCBot (Either ClientError (Maybe Illust))
fetchSearch word = runPixivInIRC "fetchSearch" $ headP (searchIllust PartialMatchForTags word (Just True) Nothing Nothing 1)

fetchRanking :: Maybe RankMode -> IRCBot (Either ClientError [Illust])
fetchRanking mode = runPixivInIRC "fetchRanking" $ getIllustRanking mode 1

fetchUserBookmark :: Text -> IRCBot (Either ClientError Illust)
fetchUserBookmark username = runPixivInIRC "fetchUserBookmark" $
  randomP $ do
    u <- headP $ searchUser username Nothing 1
    p <- (\u -> (^. _1) <$> getUserBookmarks (u ^. user . userId) Public Nothing) `traverse` u
    pure $ concat p

fetchUserBookmark' :: Int -> IRCBot (Either ClientError Illust)
fetchUserBookmark' userId = runPixivInIRC "fetchUserBookmark'" $ randomP $ (^. _1) <$> getUserBookmarks userId Public Nothing

fetchUserIllust :: Text -> IRCBot (Either ClientError Illust)
fetchUserIllust username = runPixivInIRC "fetchUserIllust" $
  randomP $ do
    u <- headP $ searchUser username Nothing 1
    p <- (\u -> getUserIllusts (u ^. user . userId) (Just TypeIllust) 1) `traverse` u
    pure $ concat p

shortenUrl :: Text -> IO Text
shortenUrl url = do
  let fc = https "fars.ee" /: "u"
  part <- reqBodyMultipart [partBS "c" $ encodeUtf8 url]
  let r = req POST fc part bsResponse mempty
  result <- runReq defaultHttpConfig r
  let s = T.drop 5 . last . T.lines . decodeUtf8 $ responseBody result
  return s

uploadPB :: FilePath -> ByteString -> IO Text
uploadPB filename bs = do
  let fc = https "fars.ee"
  part <- reqBodyMultipart [partFileRequestBody "c" filename $ RequestBodyBS bs]
  let r = req POST fc part bsResponse ("u" =: ("1" :: Text))
  result <- runReq defaultHttpConfig r
  return . decodeUtf8 $ responseBody result

useTextURI :: Text -> (forall scheme. Url scheme -> Option scheme -> a) -> Maybe a
useTextURI url f = case URI.mkURI url of
  Left _ -> Nothing
  Right uri -> case useURI uri of
    (Just (Left (http, scheme))) -> Just $ f http scheme
    (Just (Right (https, scheme))) -> Just $ f https scheme
    _ -> Nothing

getTitle :: Text -> IO (Maybe Text)
getTitle url = do
  let ua = header "user-agent" "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36"
      r = useTextURI url $ \u o -> req GET u NoReqBody bsResponse $ o <> ua
  case r of
    Just r' -> do
      result <- runReq defaultHttpConfig r'
      let parsed = S.parseTags $ responseBody result
          f = dropWhile (not . S.isTagOpenName "title") parsed
      case f of
        ((S.TagOpen "title" []) : S.TagText title : _) -> return (Just . decodeUtf8 $ title)
        _ -> return Nothing
    _ -> return Nothing

getContentTypeAndSize :: Text -> IO (Maybe (Text, Float))
getContentTypeAndSize url = do
  let r = useTextURI url $ \u o -> req GET u NoReqBody bsResponse o
  case r of
    Just r' -> do
      result <- runReq defaultHttpConfig r'
      let t = decodeUtf8 <$> responseHeader result "Content-Type"
          s = (/ 1024) . read . T.unpack . decodeUtf8 <$> responseHeader result "Content-Length"
      return $ do
        t' <- t
        s' <- s
        return (t', s')

google :: Text -> IO Text
google q = do
  let api = https "www.googleapis.com" /: "customsearch" /: "v1"
      r = req GET api NoReqBody jsonResponse $ "key" =: googleKey <> "cx" =: googleCX <> "q" =: q
  result <- runReq defaultHttpConfig r
  return $ case items $ responseBody result of
    (GoogleItem {..} : _) -> T.init $ T.unlines [title, snippet, link]
    _ -> "QAQ"

hl :: IO (Maybe (Text, Text, Text))
hl = do
  let api = http "m.laohuangli.net"
      ua = header "user-agent" "Mozilla/5.0 (Linux; Android 8.1.0; SM-J727T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.90 Mobile Safari/537.36"
      r = req GET api NoReqBody bsResponse ua
  result <- runReq defaultHttpConfig r
  let parsed = S.parseTags $ responseBody result
      f = dropWhile (\t -> not $ S.isTagOpenName "div" t && S.fromAttrib "class" t == "neirong_Yi_Ji")
      g = dropWhile (\t -> not $ S.isTagOpenName "span" t && S.fromAttrib "class" t == "txt1")
      yj = case f parsed of
        ((S.TagOpen "div" [("class", "neirong_Yi_Ji")]) : S.TagText yi : xs) -> case f xs of
          ((S.TagOpen "div" [("class", "neirong_Yi_Ji")]) : S.TagText ji : _) -> Just (yi, ji)
          _ -> Nothing
        _ -> Nothing
      date = case g parsed of
        ((S.TagOpen "span" [("class", "txt1")]) : S.TagText d : _) -> Just d
        _ -> Nothing
      k (Just (y, j)) = Just (decodeUtf8 y, decodeUtf8 j)
      k _ = Nothing
  return $ do
    d <- decodeUtf8 <$> date
    (y, j) <- k yj
    return (d, y, j)
