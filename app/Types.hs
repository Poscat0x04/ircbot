{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Types where

import Control.Concurrent.MVar
import Control.Monad.IO.Class
import Control.Monad.Reader
import Data.Aeson
import Data.ByteString (ByteString)
import Data.Text (Text)
import qualified Data.Text as T
import Data.Time (getCurrentTime)
import GHC.Conc
import GHC.Generics (Generic)
import Lens.Micro
import Network.HTTP.Client.TLS (newTlsManager)
import Network.IRC.Client
import Servant.Client (ClientEnv, ClientError)
import System.Random (randomRIO)
import Text.Pretty.Simple
import Web.Pixiv.Auth
import Web.Pixiv.Types
import Web.Pixiv.Types.PixivT
import Web.Pixiv.Utils (mkDefaultClientEnv)

type IRCBot = IRC MyState

data MyState = MyState
  { pixivState :: MVar PixivState,
    clientEnv :: ClientEnv
  }

runPixivInIRC :: String -> PixivT IO a -> IRCBot (Either ClientError a)
runPixivInIRC tag m = do
  ircState <- getIRCState
  let tVar = ircState ^. userState
  MyState {..} <- liftIO . readTVarIO $ tVar
  liftIO $ putStrLn $ "Run <" <> tag <> "> with pixiv state:"
  liftIO $ readMVar pixivState >>= pPrint
  liftIO . runClientT clientEnv . flip runReaderT pixivState $ unPixivT m

randomP :: (MonadIO m) => PixivT m [a] -> PixivT m a
randomP m = do
  xs <- m
  rand <- liftIO $ randomRIO (0, length xs -1)
  pure $ xs !! rand

headP :: (MonadIO m) => PixivT m [a] -> PixivT m (Maybe a)
headP m = do
  xs <- m
  pure $ xs ^? _head

initMyState :: ByteString -> ByteString -> IO MyState
initMyState username password = do
  manager <- liftIO newTlsManager
  clientEnv <- mkDefaultClientEnv manager
  t <- getCurrentTime
  pixivState <- liftIO $ newMVar . flip PixivState (Just "zh-CN") =<< computeTokenState manager Password {..} t
  pure MyState {..}

-----------------------------------------------------------------------------

data GoogleItem = GoogleItem
  { title :: Text,
    link :: Text,
    snippet :: Text
  }
  deriving (Show, Generic)

instance FromJSON GoogleItem

newtype GoogleResult = GoogleResult
  { items :: [GoogleItem]
  }
  deriving (Show, Generic)

instance FromJSON GoogleResult
