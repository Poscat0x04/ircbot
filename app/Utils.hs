{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# OPTIONS_GHC -ddump-deriv #-}

module Utils where

import Control.Applicative ((<|>))
import Control.Concurrent
import qualified Control.Exception as CE
import Control.Monad (forever, void)
import Control.Monad.IO.Class
import Data.Char (isDigit)
import Data.Text (Text)
import qualified Data.Text as T
import Data.Text.Encoding (decodeUtf8)
import qualified Data.Text.IO as T
import Data.Time (defaultTimeLocale, formatTime, getCurrentTime)
import qualified Network.HTTP.Client as C
import Network.HTTP.Req
import Network.IRC.Client
import Text.Pretty.Simple
import Text.Read (readMaybe)
import Types
import Web.Pixiv (getAccessToken)
import Web.Pixiv.Auth (Token)
import Web.Pixiv.Types (RankMode (..))

setupTokenRefersh :: IRCBot ()
setupTokenRefersh = forkIRC . forever $ do
  token <- runPixivInIRC "refersh token" getAccessToken
  liftIO $ putStrLn "refersh token result:"
  pPrint token
  liftIO $ threadDelay 1800000000

forkIRC :: IRC s () -> IRC s ()
forkIRC x = void $ fork x

identifyNick :: Message Text
identifyNick = Privmsg "NickServ" $ Right "identify 12345"

tryIO :: Text -> IO a -> IO (Either Text a)
tryIO s io =
  CE.try io >>= \case
    Right x -> return $ Right x
    Left (e :: HttpException) -> print e >> return (Left s)

myIRCLogger origin x = do
  now <- getCurrentTime
  T.putStrLn $
    T.unwords
      [ T.pack $ formatTime defaultTimeLocale "%c" now,
        if origin == FromServer then "--->" else "<---",
        decodeUtf8 x
      ]

joinChannels l = mapM_ (send . Join) l

extractPixivIdFromURL :: Text -> Maybe Int
extractPixivIdFromURL url =
  ((readMaybe . takeWhile isDigit . T.unpack) =<<) $
    T.stripPrefix "https://www.pixiv.net/artworks/" url
      <|> T.stripPrefix "https://www.pixiv.net/en/artworks/" url
      <|> T.stripPrefix "http://pixiv.net/i/" url
      <|> T.stripPrefix "https://www.pixiv.net/member_illust.php?mode=medium&illust_id=" url

isFc :: Text -> Bool
isFc = T.isInfixOf "fars.ee"

illegalChars :: [Char]
illegalChars = ['/', '（', '）', '：', ' ', '-', '(', ')', ':', '+', '!', '！', '、', '。']

replaceChars :: Text -> Text
replaceChars = T.map (\it -> if it `elem` illegalChars then '_' else it)

showHttpException :: HttpException -> String
showHttpException (VanillaHttpException (C.HttpExceptionRequest _ (C.StatusCodeException rsp _))) = show $ C.responseStatus rsp

anySub :: (Foldable t, Functor t) => t Text -> Text -> Bool
anySub list target = or $ fmap (`T.isInfixOf` target) list

unEither :: Either a a -> a
unEither (Left x) = x
unEither (Right x) = x

parseRankMode :: Text -> Maybe RankMode
parseRankMode = \case
  "day" -> Just Day
  "week" -> Just Week
  "month" -> Just Month
  "day-r18" -> Just DayR18
  _ -> Nothing
