{-# LANGUAGE OverloadedStrings #-}

module Config where

import Data.ByteString (ByteString)
import Data.Text

myNick :: Text
myNick = ""

myUserName :: Text
myUserName = ""

myRealName :: Text
myRealName = myUserName

myPassword :: Text
myPassword = ""

myChannels :: [Text]
myChannels =
  []

pixivUsername :: ByteString
pixivUsername = ""

pixivPassword :: ByteString
pixivPassword = ""

googleKey :: Text
googleKey = ""

googleCX :: Text
googleCX = ""
