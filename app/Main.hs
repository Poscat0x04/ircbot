{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ViewPatterns #-}

module Main where

import API
import Config
import qualified Control.Exception as CE
import Control.Monad (forM_, unless, void, when)
import Control.Monad.IO.Class (liftIO)
import Data.ByteString.Lazy (toStrict)
import Data.IORef
import Data.Maybe (fromMaybe)
import Data.Text (Text)
import qualified Data.Text as T
import GHC.IO (unsafePerformIO)
import Lens.Micro
import Network.IRC.Client
import Parser
import Servant.Client (ClientError)
import Text.Pretty.Simple
import Text.Printf (printf)
import Text.Read (readMaybe)
import Types
import Utils
import Web.Pixiv.API
import Web.Pixiv.Download
import qualified Web.Pixiv.Types as Pixiv
import qualified Web.Pixiv.Types.Lens as Pixiv
import qualified Web.Pixiv.Utils as Pixiv

main :: IO ()
main = do
  let conn =
        tlsConnection (WithDefaultConfig "chat.freenode.net" 6697)
          & logfunc .~ myIRCLogger
          & username .~ myUserName
          & realname .~ myRealName
          & onconnect .~ (defaultOnConnect >> send identifyNick >> setupTokenRefersh)

      myHandler = EventHandler (matchType _Privmsg) $ \s (_, y) -> case s of
        Channel _channelName senderName -> case y of
          Left _ -> return ()
          Right msg ->
            forkIRC $ do
              case parseMessage msg of
                -- Parse error
                Left _ -> return ()
                Right (ParsedMessage bridgedSender m) ->
                  let sender = fromMaybe senderName bridgedSender
                      replyF m = replyTo s $ sender <> ": " <> m
                      handle x = case x of
                        -- #pixiv id=...
                        [PixivID picId] -> do
                          liftIO $ writeIORef lastId picId
                          info <- fetchInfo picId
                          -- enable short url
                          sendPic replyF info True False
                        [URL (isFc -> isPb), x] -> do
                          case x of
                            --  https://fars.ee/... #pixiv id=...
                            PixivID picId -> do
                              liftIO $ writeIORef lastId picId
                              info <- fetchInfo picId
                              -- enable short url if no pb
                              sendPic replyF info (not isPb) False
                            URL url -> case extractPixivIdFromURL url of
                              --  https://fars.ee/... https://www.pixiv.net/artworks/...
                              Just picId -> do
                                liftIO $ writeIORef lastId picId
                                info <- fetchInfo picId
                                --- enable short url if no pb
                                sendPic replyF info (not isPb) False
                              _ -> return ()
                        -- A single url
                        [URL url] -> case extractPixivIdFromURL url of
                          -- This url indicates a pixiv artwork
                          Just picId -> do
                            liftIO $ writeIORef lastId picId
                            info <- fetchInfo picId
                            -- enable short url
                            sendPic replyF info True False

                          -- Unknown url
                          _ ->
                            if isFc url
                              then do
                                cts <- liftIO $ getContentTypeAndSize url
                                case cts of
                                  Just (ct, cs) -> replyF $ "⇪PB Type: " <> ct <> ", Size: " <> T.pack (printf "%.2f KiB" cs)
                                  _ -> replyF "⇪PB"
                              else do
                                result <- liftIO $ getTitle url
                                -- send title if no error
                                case result of
                                  Just title -> replyF $ "⇪Title: " <> title
                                  _ -> return ()

                        -- A single command
                        (c@(Command _ _) : xs) -> myCommandHandler replyF c >> handle xs
                        _ -> return ()
                   in liftIO (print m) >> handle m
        _ -> return ()

      cHandler = EventHandler (matchType _Notice) $ \x (_, y) -> case x of
        User "NickServ" -> when (Right ("You are now identified for \STX" <> myUserName <> "\STX.") == y) $ do
          joinChannels myChannels
          send (Nick myNick)
        _ -> return ()

      cfg =
        defaultInstanceConfig myUserName
          & handlers .~ (myHandler : cHandler : defaultEventHandlers)
  state <- initMyState pixivUsername pixivPassword
  let run = runClient conn cfg state in run `CE.catch` (\(e :: CE.SomeException) -> print e >> run)

lastId :: IORef Int
lastId = unsafePerformIO $ newIORef (-1)
{-# NOINLINE lastId #-}

-- | Handle pixiv
sendPic :: (Text -> IRCBot ()) -> Either ClientError Pixiv.Illust -> Bool -> Bool -> IRCBot ()
sendPic replyF (Right illust) enableShort sendId
  | Just url <- Pixiv.extractImageUrlsFromIllust illust ^? _head =
    do
      let isUgoira = illust ^. Pixiv.illustType == Pixiv.TypeUgoira
          illustId = illust ^. Pixiv.illustId
      -- make short url if enable
      short <-
        if enableShort && not isUgoira
          then liftIO $ (\x -> " | " <> unEither x) <$> tryIO "QAQ" (shortenUrl $ T.replace "i.pximg.net" "pximg.pixiv-viewer.workers.dev" url)
          else return ""

      let translated = fmap (\it -> "#" <> replaceChars it) $ (illust ^. Pixiv.tags) <&> (\x -> case x ^. Pixiv.translatedName of Just x -> x; Nothing -> x ^. Pixiv.name)
          tagTxt = T.intercalate " " translated
          messy = T.concat $ (\x -> x ^. Pixiv.name <> fromMaybe "" (x ^. Pixiv.translatedName)) <$> (illust ^. Pixiv.tags)

      -- send tags and short url
      replyF $ "⇪Tags: " <> tagTxt <> short <> (if Pixiv.isSinglePageIllust illust then "" else " (不止这一张~)")

      when isUgoira $ do
        result <- runPixivInIRC "processUgoira" $ do
          meta <- getUgoiraMetadata illustId
          lbs <- liftToPixivT $ downloadUgoiraToMP4 meta Nothing
          liftIO $ putStrLn $ maybe "No stderr" fst lbs
          (liftIO . uploadPB (show illustId <> ".mp4") . toStrict . snd) `traverse` lbs
        case result of
          Right (Just u) -> replyF u
          Left err -> pPrint err >> replyF "failed to process ugoira"

      -- 性癖
      when (anySub ["漏尿", "放尿", "お漏らし", "おもらし", "おしっこ"] messy) $
        replyF "⇪ #oc诱捕器"

      when (anySub ["調教", "束縛", "機械姦", "緊縛", "縛り", "鼻フック", "監禁", "口枷"] messy) $
        replyF "⇪ #空指针诱捕器"

      when sendId $
        replyF $ "⇪ Pixiv id: " <> T.pack (show illustId)
sendPic replyF (Left err) _ _ = pPrint err >> replyF (T.pack $ show err)

-- | Handle command
myCommandHandler :: (Text -> IRCBot ()) -> Entry -> IRCBot ()
myCommandHandler replyF (Command cmd args) = case cmd of
  "ping" -> replyF "pong"
  "echo" -> replyF args
  "pixiv" -> case readMaybe @Int (T.unpack args) of
    Just picId -> do
      liftIO $ writeIORef lastId picId
      info <- fetchInfo picId
      sendPic replyF info True True
    _ -> replyF $ "Unable to parse pixiv id " <> args
  "tags" -> case readMaybe @Int (T.unpack args) of
    Just picId -> do
      liftIO $ writeIORef lastId picId
      info <- fetchInfo picId
      sendPic replyF info False False
    _ -> replyF $ "Unable to parse pixiv id " <> args
  "related" -> do
    pixivId <- liftIO $ readIORef lastId
    result <- fetchRandomRelated pixivId
    sendPic replyF result True True
  "last" -> do
    pixivId <- liftIO $ readIORef lastId
    result <- fetchInfo pixivId
    sendPic replyF result False False
  "source" -> replyF "https://gitlab.com/berberman/ircbot"
  "google" -> do
    if T.null args
      then replyF "need args"
      else do
        result <- liftIO $ tryIO "QAQ" (google args)
        replyF $ unEither result
  "today" -> do
    result <- liftIO hl
    case result of
      Just (d, y, j) -> do
        replyF d
        replyF $ "宜:" <> y
        replyF $ "忌:" <> j
      _ -> replyF "QAQ"
  "search" -> do
    result <- fetchSearch args
    case result of
      Right (Just pic) -> sendPic replyF (Right pic) True True
      _ -> replyF $ "Unable to find \"" <> args <> "\""
  "ranking" -> do
    result <- fetchRanking $ parseRankMode args
    case result of
      Right x -> forM_ x $ \r -> do
        sendPic replyF (Right r) True True
      Left err -> replyF "Unable to get ranking"
  "bookmark" -> do
    result <- fetchUserBookmark args
    sendPic replyF result True True
  "bookmark'" -> case readMaybe @Int (T.unpack args) of
    Just userId -> do
      result <- fetchUserBookmark' userId
      sendPic replyF result True True
    _ -> replyF $ "Unable to parse user id " <> args
  "work" -> do
    result <- fetchUserIllust args
    sendPic replyF result True True
  "help" ->
    replyF $
      T.unwords
        [ "'ping",
          "'echo [...]",
          "'pixiv [illustId]",
          "'tags [illustId]",
          "'related",
          "'last",
          "'source",
          "'google [keywords]",
          "'today",
          "'search [tag]",
          "'ranking [day|week|month|day-r18]",
          "'bookmark [username]",
          "'bookmark' [userId]",
          "'work [username] ",
          "'help"
        ]
  _ -> return ()
