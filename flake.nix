{
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  inputs.pixiv.url = "github:poscat0x04/pixiv";

  outputs = { self, nixpkgs, pixiv, ... }:
    let
      pkgs = import nixpkgs {
        system = "x86_64-linux";
        overlays = [ self.overlay pixiv.overlay ];
      };
    in with pkgs; {
      overlay = self: super:
        let
          hpkgs = super.haskellPackages;
          ircbot = hpkgs.callCabal2nix "ircbot" ./. { };
        in with super;
        with haskell.lib; {
          inherit ircbot;
          ircbot-dev =
            addBuildTools ircbot [ haskell-language-server cabal-install ];
        };
      defaultPackage.x86_64-linux = ircbot;
      devShell.x86_64-linux = ircbot-dev.envFunc { };
    };
}
